input_year = input("Please input a year: \n")
try:
    if int(input_year) % 4 == 0:
        print(f"{input_year} is a leap year")
    elif int(input_year) <= 0:
        print("Zero is not allowed")
    else: 
        print(f"{input_year} is not a leap year")
except ValueError:
    print("String is not allowed")


print("-------------------------")
input_row = int(input("Enter number of rows \n"))
input_col = int(input("Enter number of columns \n"))
for x in range(input_row):
    output = ""
    for y in range(input_col):
        output += "*"
    print(output)